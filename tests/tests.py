from os.path import basename, abspath, join, dirname

import django
import requests
from django.test import TestCase
from django.utils.baseconv import base64


class TestTransferUserPhoto(TestCase):

    def setUp(self):
        self.url = 'http://127.0.0.1/photo'
        self.images_folder = dirname(__file__)

    def test_submit_photo(self):
        image_file = abspath(join(self.images_folder, 'test_img_01.jpg'))
        with open(image_file, 'rb') as f:
            data = f.read()
            data_url = 'data:image/jpg;base64,%s' % base64.b64encode(data)
            data_url = data_url[0:22] + data_url[24:]
            print(data_url[0:50])
            r = requests.post(self.url, data={'img': data_url, 'place': 'default'})
        print(r)
