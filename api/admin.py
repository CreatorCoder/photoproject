from django.contrib import admin
from .models import Photo


class PhotoAdmin(admin.ModelAdmin):
    fields = ('image_tag', 'image', 'image_size', 'add_date')
    readonly_fields = ('image_tag',)


admin.site.register(Photo, PhotoAdmin)
