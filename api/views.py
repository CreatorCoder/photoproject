from os.path import join, abspath
from django.conf import settings
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from api.models import Photo, HotelRoom
from api.serializers import UserPhotoSerializer


class UserListPhotoViewSet(viewsets.ModelViewSet):
    serializer_class = UserPhotoSerializer

    def get_queryset(self):
        queryset = Photo.objects.all()
        size = self.request.query_params.get('size', None)
        if size is not None:
            queryset = queryset.filter(image_size=int(size))
        date = self.request.query_params.get('date', None)
        if date is not None:
            queryset = queryset.filter(date=int(date))
        return queryset


@api_view(['POST'])
def add_photo(request):
    place = request.POST.get('place')

    file = request.FILES['file']

    if place == 'default':
        place = settings.MEDIA_ROOT
        path_to_image_file = abspath(join(place, file.name))
        with open(path_to_image_file, 'wb') as f:
            f.write(file.read())
            f.close()
        new_photo = Photo()
        new_photo.image_size = file.size
        new_photo.image = path_to_image_file
        new_photo.save()
    else:
        return Response(status=500)

    return Response(status=200)


def get_rooms(request):
    from django.db.models import Count
    queryset = HotelRoom.objects.all().values('hotel_id').annotate(total=Count('hotel_id'))
    queryset.all().filter(total__lt=20)


