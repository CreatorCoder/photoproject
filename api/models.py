import os
from django.db import models
from django.utils.safestring import mark_safe
from PhotoArchive import settings
from django.utils import timezone


class Photo(models.Model):
    add_date = models.DateField(default=timezone.now, blank=True)
    image_size = models.IntegerField()
    image = models.ImageField(upload_to=settings.MEDIA_ROOT, blank=True, null=True)

    def url(self):
        return os.path.join('/', settings.MEDIA_URL, os.path.basename(str(self.image)))

    def image_tag(self):
        return mark_safe('<img src="{}" width="150" height="150" />'.format(self.url()))

    image_tag.short_description = 'Image'

    class Meta:
        db_table = 'users_photos'


class HotelRoom(models.Model):
    id = models.AutoField(primary_key=True)
    room_id = models.IntegerField(null=False)
    hotel_id = models.IntegerField(null=False)
    price = models.IntegerField(default=0)

    class Meta:
        db_table = 'hotelrooms'
