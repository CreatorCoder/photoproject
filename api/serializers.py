from rest_framework import serializers
from api.models import Photo


class UserPhotoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Photo
        fields = ('url', 'add_date', 'image_size')
