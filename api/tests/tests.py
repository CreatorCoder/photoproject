from os.path import abspath, join, dirname
from django.conf import settings
from django.test import TestCase
import psycopg2
import requests
import os
import json


class TestTransferUserPhoto(TestCase):

    def setUp(self):
        self.url = 'http://127.0.0.1:8000/'
        self.images_folder = dirname(__file__)
        self.conn = psycopg2.connect(dbname='photo_db', user='postgres', password='ghgH95Gfgsdfs',
                                     host='localhost', port=5432)

    def clear_db(self):
        cur = self.conn.cursor()
        cur.execute('DELETE FROM public.users_photos')
        self.conn.commit()

    def test_submit_photo(self):
        self.clear_db()

        image_file = abspath(join(self.images_folder, 'test_img_01.jpg'))

        try:
            with open(image_file, 'rb') as fin:
                files = {'file': fin}
                url = '{}photo/'.format(self.url)
                r = requests.post(url, files=files, data={'place': 'default'})
                print(r.text)
        except Exception as er:
            self.assertTrue(False)

        self.assertTrue(os.path.isfile(abspath(join(settings.MEDIA_ROOT, 'test_img_01.jpg'))))

        cur = self.conn.cursor()
        image_file = abspath(join(settings.MEDIA_ROOT, 'test_img_01.jpg'))
        cur.execute("SELECT * FROM public.users_photos WHERE image='{0}';".format(r'{}'.format(image_file)))
        result_photos = cur.fetchall()
        self.assertTrue(len(result_photos) > 0)

    def test_get_photos(self):

        self.clear_db()

        image_file = abspath(join(self.images_folder, 'test_img_01.jpg'))

        try:
            with open(image_file, 'rb') as fin:
                files = {'file': fin}
                url = '{}photo/'.format(self.url)
                r = requests.post(url, files=files, data={'place': 'default'})
        except Exception as er:
            self.assertTrue(False)

        url = '{}photos/'.format(self.url)
        raw_result = requests.get(url)
        json_result = json.loads(raw_result.text)
        self.assertTrue(len(json_result) > 0)
