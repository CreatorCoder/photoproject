import base64
from os.path import dirname, abspath, join
import requests

url = 'http://127.0.0.1:8000/photo'
images_folder = dirname(__file__)


image_file = abspath(join(images_folder, 'test_img_01.jpg'))

with open(image_file, 'rb') as f:
    data = f.read()
    data_url = 'data:image/jpg;base64,%s' % base64.b64encode(data)
    data_url = data_url[0:22] + data_url[24:]
    print(data_url[0:50])
    r = requests.post(url, data={'img': data_url, 'place': 'default'})
print(r)